# Euler

<img src="./icon.png" height="100px" />

My solutions to [Project Euler](https://projecteuler.net) problems.

## Solutions

| Problem | Answer | Languages |
| ------- | ------ | --------- |
| [Problem 1 – Multiples of 3 and 5](https://projecteuler.net/problem=1) | 233168 | [C#](./csharp/Euler/Centum01/Decem01/Problem001.cs), [Kotlin](./kotlin/src/Problem001.kt), [Python](./python/problem001.py) |
| [Problem 2 – Even Fibonacci numbers](https://projecteuler.net/problem=2) | 4613732 | [C#](./csharp/Euler/Centum01/Decem01/Problem002.cs), [Kotlin](./kotlin/src/Problem002.kt), [Python](./python/problem002.py) |
| [Problem 3 – Largest prime factor](https://projecteuler.net/problem=3) | 6857 | [C#](./csharp/Euler/Centum01/Decem01/Problem003.cs), [Kotlin](./kotlin/src/Problem003.kt), [Python](./python/problem003.py) |
| [Problem 4 – Largest palindrome product](https://projecteuler.net/problem=4) | 906609 | [C#](./csharp/Euler/Centum01/Decem01/Problem004.cs), [Kotlin](./kotlin/src/Problem004.kt), [Python](./python/problem004.py) |
| [Problem 5 – Smallest multiple](https://projecteuler.net/problem=5) | 232792560 | [C#](./csharp/Euler/Centum01/Decem01/Problem005.cs), [Kotlin](./kotlin/src/Problem005.kt), [Python](./python/problem005.py) |
| [Problem 6 – Sum square difference](https://projecteuler.net/problem=6) | 25164150 | [C#](./csharp/Euler/Centum01/Decem01/Problem006.cs), [Kotlin](./kotlin/src/Problem006.kt) |
| [Problem 7 – 10001st prime](https://projecteuler.net/problem=7) | 104743 | [C#](./csharp/Euler/Centum01/Decem01/Problem007.cs) |
| [Problem 8 – Largest product in a series](https://projecteuler.net/problem=8) | 23514624000 | [C#](./csharp/Euler/Centum01/Decem01/Problem008.cs) |
| [Problem 9 – Special Pythagorean triplet](https://projecteuler.net/problem=9) | 31875000 | [C#](./csharp/Euler/Centum01/Decem01/Problem009.cs) |
| [Problem 10 – Summation of primes](https://projecteuler.net/problem=10) | 142913828922 | [C#](./csharp/Euler/Centum01/Decem01/Problem010.cs) |
| [Problem 11 – Largest product in a grid](https://projecteuler.net/problem=11) | 70600674 | [C#](./csharp/Euler/Centum01/Decem02/Problem011.cs) |
| [Problem 12 – Highly divisible triangular number](https://projecteuler.net/problem=12) | 76576500 | [C#](./csharp/Euler/Centum01/Decem02/Problem012.cs) |
| [Problem 13 – Large sum](https://projecteuler.net/problem=13) | 5537376230 | [C#](./csharp/Euler/Centum01/Decem02/Problem013.cs) |
| [Problem 14 – Longest Collatz sequence](https://projecteuler.net/problem=14) | 837799 | [C#](./csharp/Euler/Centum01/Decem02/Problem014.cs) |
| [Problem 15 – Lattice paths](https://projecteuler.net/problem=15) | 137846528820 | [C#](./csharp/Euler/Centum01/Decem02/Problem015.cs) |
| [Problem 16 – Power digit sum](https://projecteuler.net/problem=16) | 1366 | [C#](./csharp/Euler/Centum01/Decem02/Problem016.cs) |
| [Problem 17 – Number letter counts](https://projecteuler.net/problem=17) | 21124 | [C#](./csharp/Euler/Centum01/Decem02/Problem017.cs) |
| [Problem 18 – Maximum path sum I](https://projecteuler.net/problem=18) | 1074 | [C#](./csharp/Euler/Centum01/Decem02/Problem018.cs) |
| [Problem 19 – Counting Sundays](https://projecteuler.net/problem=19) | 171 | [C#](./csharp/Euler/Centum01/Decem02/Problem019.cs) |
| [Problem 20 – Factorial digit sum](https://projecteuler.net/problem=20) | 648 | [C#](./csharp/Euler/Centum01/Decem02/Problem020.cs) |
| [Problem 21 – Amicable numbers](https://projecteuler.net/problem=21) | 31626 | [C#](./csharp/Euler/Centum01/Decem03/Problem021.cs) |
| [Problem 22 – Names scores](https://projecteuler.net/problem=22) | 871198282 | [C#](./csharp/Euler/Centum01/Decem03/Problem022.cs) |
| [Problem 23 – Non-abundant sums](https://projecteuler.net/problem=23) | 4179871 | [C#](./csharp/Euler/Centum01/Decem03/Problem023.cs) |
| [Problem 24 – Lexicographic permutations](https://projecteuler.net/problem=24) | 2783915460 | [C#](./csharp/Euler/Centum01/Decem03/Problem024.cs) |
| [Problem 25 – 1000-digit Fibonacci number](https://projecteuler.net/problem=25) | 4782 | [C#](./csharp/Euler/Centum01/Decem03/Problem025.cs) |
| [Problem 26 – Reciprocal cycles](https://projecteuler.net/problem=26) | 983 | [C#](./csharp/Euler/Centum01/Decem03/Problem026.cs) |
| [Problem 27 – Quadratic primes](https://projecteuler.net/problem=27) | -59231 | [C#](./csharp/Euler/Centum01/Decem03/Problem027.cs) |
| [Problem 28 – Number spiral diagonals](https://projecteuler.net/problem=28) | 669171001 | [C#](./csharp/Euler/Centum01/Decem03/Problem028.cs) |
| [Problem 29 – Distinct powers](https://projecteuler.net/problem=29) | 9183 | [C#](./csharp/Euler/Centum01/Decem03/Problem029.cs) |
| [Problem 30 – Digit fifth powers](https://projecteuler.net/problem=30) | 443839 | [C#](./csharp/Euler/Centum01/Decem03/Problem030.cs) |
| [Problem 31 – Coin sums](https://projecteuler.net/problem=31) | 73682 | [C#](./csharp/Euler/Centum01/Decem04/Problem031.cs) |
| [Problem 32 – Pandigital products](https://projecteuler.net/problem=32) | 45228 | [C#](./csharp/Euler/Centum01/Decem04/Problem032.cs) |
| [Problem 33 – Digit cancelling fractions](https://projecteuler.net/problem=33) | 100 | [C#](./csharp/Euler/Centum01/Decem04/Problem033.cs) |
| [Problem 34 – Digit factorials](https://projecteuler.net/problem=34) | 40730 | [C#](./csharp/Euler/Centum01/Decem04/Problem034.cs) |
| [Problem 35 – Circular primes](https://projecteuler.net/problem=35) | 55 | [C#](./csharp/Euler/Centum01/Decem04/Problem035.cs) |
| [Problem 36 – Double-base palindromes](https://projecteuler.net/problem=36) | 872187 | [C#](./csharp/Euler/Centum01/Decem04/Problem036.cs) |
| [Problem 37 – Truncatable primes](https://projecteuler.net/problem=37) | 748317 | [C#](./csharp/Euler/Centum01/Decem04/Problem037.cs) |
| [Problem 38 – Pandigital multiples](https://projecteuler.net/problem=38) | 932718654 | [C#](./csharp/Euler/Centum01/Decem04/Problem038.cs) |
| [Problem 39 – Integer right triangles](https://projecteuler.net/problem=39) | 840 | [C#](./csharp/Euler/Centum01/Decem04/Problem039.cs) |
| [Problem 40 – Champernowne's constant](https://projecteuler.net/problem=40) | 210 | [C#](./csharp/Euler/Centum01/Decem04/Problem040.cs) |
| [Problem 41 – Pandigital prime](https://projecteuler.net/problem=41) | 7652413 | [C#](./csharp/Euler/Centum01/Decem05/Problem041.cs) |
| [Problem 42 – Coded triangle numbers](https://projecteuler.net/problem=42) | 162 | [C#](./csharp/Euler/Centum01/Decem05/Problem042.cs) |
| [Problem 43 – Sub-string divisibility](https://projecteuler.net/problem=43) | 16695334890 | [C#](./csharp/Euler/Centum01/Decem05/Problem043.cs) |

## License

![AGPLv3](agplv3.png)

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [LICENSE.txt](LICENSE.txt) for details.

## Credits

Project icon made by [Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)
