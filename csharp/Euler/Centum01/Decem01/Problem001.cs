// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

namespace Euler.Centum01.Decem01
{
    public static class Problem001
    {
        public static int Solve()
        {
            int max3 = MaxMultBelow(3, 1000);
            int max5 = MaxMultBelow(5, 1000);
            int max15 = MaxMultBelow(15, 1000);

            int sum3 = 3 * SumIdentity(max3);
            int sum5 = 5 * SumIdentity(max5);
            int sum15 = 15 * SumIdentity(max15);

            return sum3 + sum5 - sum15;
        }

        /// <summary>
        /// Gets the max mutiple of <paramref name="n"/> lower than <paramref name="max"/>.
        /// </summary>
        private static int MaxMultBelow(int n, int max) => (max - 1) / n;

        private static int SumIdentity(int n) => n * (n + 1) / 2;
    }
}
