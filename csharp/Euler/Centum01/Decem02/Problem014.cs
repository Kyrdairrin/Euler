// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System.Collections.Generic;

namespace Euler.Centum01.Decem02
{
    public static class Problem014
    {
        private static readonly Dictionary<long, List<long>> _alreadyChecked = new Dictionary<long, List<long>>();

        public static int Solve()
        {
            int bestStart = 1;
            int longest = 1;

            for (int i = 1; i <= 1000000; i++)
            {
                var sequence = CollatzSequence(i);
                if (sequence.Count <= longest) continue;
                longest = sequence.Count;
                bestStart = i;
            }

            return bestStart;
        }

        private static List<long> CollatzSequence(int n)
        {
            var sequence = new List<long> { n };

            long num = n;

            while (num > 1)
            {
                num = NextCollatz(num);
                if (_alreadyChecked.ContainsKey(num))
                {
                    sequence.AddRange(_alreadyChecked[num]);
                    break;
                }
                sequence.Add(num);
            }

            _alreadyChecked.Add(n, sequence);

            return sequence;
        }

        private static long NextCollatz(long n)
        {
            if (n % 2 == 0) return n / 2;
            return 3 * n + 1;
        }
    }
}