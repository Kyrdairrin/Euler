// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System.Linq;
using System.Text.RegularExpressions;

namespace Euler.Centum01.Decem02
{
    public static class Problem017
    {
        public static int Solve()
        {
            var whitespaces = new Regex(@"\s+");
            return Enumerable.Range(1, 1000).Select(NumberToWords).Select(str => whitespaces.Replace(str, "")).Sum(i => i.Length);
        }

        private static string NumberToWords(int n)
        {
            string words = "";

            if (n / 1000 > 0)
            {
                words += $"{NumberToWords(n / 1000)} thousand ";
                n %= 1000;
            }

            if (n / 100 > 0)
            {
                words += $"{NumberToWords(n / 100)} hundred ";
                n %= 100;
            }

            if (n <= 0) return words;

            if (words != "") words += "and ";

            string[] units = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            string[] tens = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

            if (n < 20) words += units[n];
            else
            {
                words += tens[n / 10];
                if (n % 10 > 0) words += $" {units[n % 10]}";
            }

            return words;
        }
    }
}