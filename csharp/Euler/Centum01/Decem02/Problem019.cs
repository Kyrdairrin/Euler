// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System;

namespace Euler.Centum01.Decem02
{
    public static class Problem019
    {
        public static int Solve()
        {
            int count = 0;
            for (int i = 1901; i <= 2000; i++)
            {
                for (int j = 1; j <= 12; j++)
                {
                    if (Functions.ZellerGregorian(i, j, 1) == DayOfWeek.Sunday) count++;
                }
            }
            return count;
        }
    }
}