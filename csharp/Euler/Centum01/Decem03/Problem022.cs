// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Euler.Centum01.Decem03
{
    public static class Problem022
    {
        public static int Solve()
        {
            List<string> names = File.ReadAllText("./Centum01/Decem03/p022_names.txt").Split(new[] { '"', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            names.Sort();
            return names.Select((s, i) => s.ToAlphabeticalValue() * (i + 1)).Sum();
        }
    }
}