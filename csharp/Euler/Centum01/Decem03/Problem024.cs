// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System.Collections.Generic;
using static Euler.Functions;

namespace Euler.Centum01.Decem03
{
    public static class Problem024
    {
        /*
         === MY THOUGHT PROCESS ===

         There are n! permutations (=> bruteforce is a bad idea)
         It means there are 9! permutations if we remove the 0 (i.e. the 9! first permutations starts with 0)
         In the same way, there are 9! permutations starting with each other digit.

         OK, than we now that 2*9! = 725 760 and 3*9! = 1 088 640.
         We want N° 1 000 000. With the previous results, we know it starts with 2 (permutations starting with 2 are between the 2 previous values).

         I think the process can be repeated.
         We now have our first digit (2). There are 8! sub-permutations starting with 0, 8! starting with 1...
         We find that 2*9! + 6*8! = 967680 and that 2*9! + 7*8! = 1 008 000
         So the sub-permutation following the 2 starts with the 6th remaining digit (all except 2, because it's already used), i.e. 7
         We now have 27 as the first 2 gigits or out 1 000 000th permutation.

         We repeat the process...

        */

        public static string Solve()
        {
            List<int> availableDigits = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] permutation = new int[10];
            int count = 0;

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < availableDigits.Count; j++)
                {
                    if (count + (j + 1) * Factorial(9 - i) >= 1000000)
                    {
                        count += j * (int)Factorial(9 - i);
                        permutation[i] = availableDigits.GetAndRemoveAt(j);
                        break;
                    }
                }
            }

            return string.Join("", permutation);
        }
    }
}