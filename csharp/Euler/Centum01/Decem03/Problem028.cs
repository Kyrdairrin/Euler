// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

namespace Euler.Centum01.Decem03
{
    public static class Problem028
    {
        /*
         === MY THOUGHT PROCESS ===

         Top right = n²
         Top left = n² - (n + 1)
         Bottom left = n² - 2 * (n + 1)
         Bottom right = n² - 3 * (n + 1)

         ==> All corners = 4n² - 6 * (n - 1)

         ==> diag sum = 1 + sum of all (4n² - 6 * (n - 1)) with n is 3, 5, 7, ..., size
        */

        public static int Solve()
        {
            int sum = 1;
            for (int n = 3; n <= 1001; n += 2)
            {
                sum += 4 * n * n - 6 * (n - 1);
            }
            return sum;
        }
    }
}