// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

namespace Euler.Centum01.Decem04
{
    public static class Problem031
    {
        public static int Solve()
        {
            int count = 0;

            for (int pound2 = 0; pound2 <= 200; pound2+=200)
            {
                for (int pound1 = pound2; pound1 <= 200; pound1+=100)
                {
                    for (int pence50 = pound1; pence50 <= 200; pence50+=50)
                    {
                        for (int pence20 = pence50; pence20 <= 200; pence20+=20)
                        {
                            for (int pence10 = pence20; pence10 <= 200; pence10+=10)
                            {
                                for (int pence5 = pence10; pence5 <= 200; pence5+=5)
                                {
                                    for (int pence2 = pence5; pence2 <= 200; pence2+=2)
                                    {
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return count;
        }
    }
}