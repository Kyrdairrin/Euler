// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

namespace Euler.Centum01.Decem04
{
    public static class Problem032
    {
        public static int Solve()
        {
            int count = 0;
            for (int p = 1; p < 10000; p++) // if p = 10000, there is 4 remainig digits. but 99*99 < 10000, then max_p < 10000
            {
                for (int a = 1; a <= p; a++)
                {
                    if (p % a == 0 && Functions.IsPandigital($"{a}{p/a}{p}"))
                    {
                        count += p;
                        break;
                    }
                }
            }
            return count;
        }
    }
}