// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using static Euler.Functions;

namespace Euler.Centum01.Decem04
{
    public static class Problem033
    {
        public static BigInteger Solve()
        {
            var digitCancellingFractions = new List<(BigInteger num, BigInteger denum)>();

            for (int denum = 12; denum < 100; denum++)
            {
                for (int num = 11; num < denum; num++)
                {
                    if (num % 10 == denum / 10 && ReduceFraction(num, denum) == ReduceFraction(num / 10, denum % 10))
                    {
                        digitCancellingFractions.Add((num, denum));
                    }
                }
            }

            return ReduceFraction(
                digitCancellingFractions.Select(f => f.num).Aggregate(BigInteger.One, BigInteger.Multiply),
                digitCancellingFractions.Select(f => f.denum).Aggregate(BigInteger.One, BigInteger.Multiply)).denum;
        }
    }
}
