// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System.Numerics;
using static Euler.Functions;

namespace Euler.Centum01.Decem04
{
    public static class Problem038
    {
        public static BigInteger Solve()
        {
            BigInteger max_p = 0;

            for (int n = 2; n <= 9; n++)
            {
                // since n > 1, num has at most 4 digits
                for (int num = 0; num < 9999; num++)
                {
                    BigInteger p = ConcatenatedProduct(num, n);
                    if(IsPandigital(p)) max_p = BigInteger.Max(max_p, p);
                }
            }

            return max_p;
        }
    }
}
