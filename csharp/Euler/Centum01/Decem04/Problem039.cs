// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using static Euler.Functions;

namespace Euler.Centum01.Decem04
{
    public static class Problem039
    {
        public static int Solve()
        {
            int maxP = 0;
            int maxCount = 0;
            for (int p = 4; p <= 1000; p++)
            {
                int count = 0;
                for (int a = 1; a < p; a++)
                {
                    for (int b = a; b < p - a; b++)
                    {
                        if (IsPythagoreanTriplet(a, b, p - a - b)) count++;
                    }
                }
                if (count > maxCount)
                {
                    maxP = p;
                    maxCount = count;
                }
            }
            return maxP;
        }
    }
}
