﻿// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System;
using static System.Math;
using static Euler.Functions;

namespace Euler.Centum01.Decem05
{
    public static class Problem041
    {
        public static int Solve()
        {
            // be 1 + 2 + ... + n = X
            // if X is divisible by 3 ==> all n-pandigital numbers are divisible by 3 and not prime
            // only 4- and 7-pandigital numbers are not divisible by 3 and may be prime

            for (int n = 7; n >= 4; n-=3)
            {
                for (int i = (int)Pow(10, n) - 1; i > 0; i -= 2)
                {
                    if (IsPandigital(i, 1, n) && IsPrime(i)) return i;
                }
            }

            throw new InvalidOperationException("Not found");
        }
    }
}
