﻿// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using System.Numerics;
using static Euler.Functions;

namespace Euler.Centum01.Decem05
{
    public static class Problem043
    {
        public static BigInteger Solve()
        {
            BigInteger sum = 0;

            foreach (BigInteger num in 9876543210.Permutations())
            {
                if (IsPandigital(num, 0) && IsSubDivisible(num)) sum += num;
            }

            return sum;
        }

        private static bool IsSubDivisible(BigInteger n)
            => Sub(n, 2) % 2 == 0
                && Sub(n, 3) % 3 == 0
                && Sub(n, 4) % 5 == 0
                && Sub(n, 5) % 7 == 0
                && Sub(n, 6) % 11 == 0
                && Sub(n, 7) % 13 == 0
                && Sub(n, 8) % 17 == 0;

        private static long Sub(BigInteger n, int start) => int.Parse($"{n}".Substring(start - 1, 3));
    }
}
