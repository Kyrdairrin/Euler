// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using Supernova.Foundation.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Euler
{
    public static class Functions
    {
        private static readonly Dictionary<BigInteger, BigInteger> _factorialsCache = new Dictionary<BigInteger, BigInteger>
        {
            [0] = 1,
            [1] = 1,
            [2] = 2,
            [3] = 6,
            [4] = 24,
            [5] = 120,
            [6] = 720,
            [7] = 5040,
            [8] = 40320,
            [9] = 362880,
            [10] = 3628800
        };

        public static BigInteger BinomialCoefficient(int n, int k)
        {
            if (k < 0 || n < 0 || k > n) return 0;
            return Factorial(n) / (Factorial(k) * Factorial(n - k));
        }

        /// <summary>
        /// Checks the number perfection :
        /// - returns 0 if num is a perfect number, i.e. sum(proper divisors) = num
        /// - returns 1 if num is an abundant number, i.e. sum(proper divisors) > num
        /// - returns -1 if num is a deficient number, i.e. sum(proper divisors) < num
        /// </summary>
        /// <param name="num">The number</param>
        /// <returns>The result</returns>
        public static int CheckNumberPerfection(int num) => ProperDivisors(num).Sum().CompareTo(num);

        /// <summary>
        /// Returns the concatenated product of a number <paramref name="num"/> and a range (1, 2, ..., <paramref name="n"/>).
        /// </summary>
        /// <param name="num">A number.</param>
        /// <param name="n">The range limit.</param>
        /// <returns>The concatenated product.</returns>
        public static BigInteger ConcatenatedProduct(BigInteger num, BigInteger n)
        {
            var builder = new StringBuilder();

            for (int i = 1; i <= n; i++)
            {
                builder.Append(num * i);
            }

            return BigInteger.Parse(builder.ToString());
        }

        public static int CountDivisors(int num)
        {
            int count = 0;
            int max = (int)Math.Sqrt(num);

            for (int i = 1; i <= max; i++)
            {
                if (num % i == 0)
                    count += 2;
            }

            if (num == max * max) count--;

            return count;
        }

        public static BigInteger[] CyclicPermutations(this int n) => CyclicPermutations(new BigInteger(n));

        public static BigInteger[] CyclicPermutations(this BigInteger n)
        {
            if (n < 0) return CyclicPermutations(-n).Select(i => -i).ToArray();
            if (n < 10) return new[] { n };

            return n.Digits().CyclicPermutations().Select(ToBigInteger).ToArray();
        }

        public static IEnumerable<IEnumerable<T>> CyclicPermutations<T>(this T[] array)
        {
            Guard.NotNullOrEmpty(array, nameof(array));

            if (array.Length == 1)
            {
                yield return array;
                yield break;
            }

            for (int i = 0; i < array.Length; i++)
            {
                yield return array.Skip(i).Concat(array.Take(i));
            }
        }

        public static int[] Digits(this BigInteger n) => n.ToString().Select(d => d - '0').ToArray();

        public static BigInteger Factorial(BigInteger n)
        {
            Guard.Positive(n, nameof(n));

            if (n < _factorialsCache.Count) return _factorialsCache[(int)n];

            BigInteger fac = n * Factorial(n - 1);
            _factorialsCache.Add(n, fac);
            return fac;
        }

        public static IEnumerable<BigInteger> Fibonacci()
        {
            yield return 0;
            yield return 1;

            BigInteger a = 0;
            BigInteger b = 1;
            while (true)
            {
                BigInteger sum = a + b;
                yield return sum;
                a = b;
                b = sum;
            }
        }

        public static BigInteger GCD(BigInteger a, BigInteger b) => b == 0 ? a : GCD(b, a % b);

        public static List<int> GetAbundantNumbers(int max)
        {
            var numbers = new List<int>();
            for (int i = 12; i <= max; i++) // 12 is the smallest abundant number
            {
                if (CheckNumberPerfection(i) > 0) numbers.Add(i);
            }
            return numbers;
        }

        public static T GetAndRemoveAt<T>(this IList<T> list, int index)
        {
            T value = list[index];
            list.RemoveAt(index);
            return value;
        }

        // See https://www.quora.com/What-determines-the-number-of-digits-for-recurring-decimals/answer/Sridhar-Ramesh
        public static int GetFractionRecurringCycleLength(BigInteger num, BigInteger denum)
        {
            (_, denum) = ReduceFraction(num, denum);

            var denumfactors = PrimeDecomposition(denum);
            int t = denumfactors
                .Where(factor => factor.Key != 2 && factor.Key != 5)
                .Aggregate(1, (current, factor) => current * (int)BigInteger.Pow(factor.Key, factor.Value));

            BigInteger n = 9;
            while (true)
            {
                if (n % t == 0) return n.ToString().Length;
                n = 10 * n + 9;
            }
        }

        public static bool IsAmicable(int a)
        {
            int b = ProperDivisors(a).Sum();
            return b != a && ProperDivisors(b).Sum() == a;
        }

        public static bool IsPalindromic(string str) => str.SequenceEqual(str.Reverse());

        public static bool IsPalindromic(int num) => IsPalindromic($"{num}");

        public static bool IsPandigital(string num, int n_min = 1, int n_max = 9)
            => num.OrderBy(c => c).SequenceEqual(new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }.Skip(n_min).Take(n_max - n_min + 1));

        public static bool IsPandigital(BigInteger num, int n_min = 1, int n_max = 9) => IsPandigital(num.ToString(), n_min, n_max);

        public static bool IsPrime(int num) => IsPrime(new BigInteger(num));

        public static bool IsPrime(BigInteger num)
        {
            if (num < 2) return false;
            if (num == 2 || num == 3) return true;
            if (num % 2 == 0) return false;

            for (int i = 3; ; i += 2)
            {
                BigInteger q = num / i;
                if (num == q * i) return false;
                if (q < i) return true;
            }
        }

        public static bool IsPythagoreanTriplet(int a, int b, int c) => a < b && b < c && a * a + b * b == c * c;

        public static bool IsTriangular(int num) => Math.Sqrt(8 * num + 1) % 1 == 0; // checks if 8num+1 is a perfect square

        public static BigInteger LCM(params BigInteger[] numbers)
        {
            BigInteger a = numbers[0];
            BigInteger b = numbers[1];
            var rest = numbers.Skip(2);

            BigInteger abLCM = a * b / GCD(a, b);

            if (!rest.Any()) return abLCM;

            return LCM(new[] { abLCM }.Concat(rest).ToArray());
        }

        public static IEnumerable<BigInteger> LeftTruncations(this BigInteger n)
        {
            int[] digits = n.Digits();
            for (int i = 1; i < digits.Length; i++)
            {
                yield return digits.Skip(i).ToBigInteger();
            }
        }

        public static BigInteger NextPrime(BigInteger prime)
        {
            if (prime == 2) return 3;
            if (prime % 2 == 0) prime++;

            do
            {
                prime += 2;
            } while (!IsPrime(prime));

            return prime;
        }

        public static List<BigInteger> Permutations(this BigInteger num) => num.Digits().Permutations().Select(ToBigInteger).ToList();

        public static List<BigInteger> Permutations(this int num) => Permutations(new BigInteger(num));

        public static List<BigInteger> Permutations(this long num) => Permutations(new BigInteger(num));

        public static List<List<T>> Permutations<T>(this IList<T> list)
        {
            var permutations = new List<List<T>>();

            HeapPermutation(list, list.Count);

            void HeapPermutation(IList<T> source, int n)
            {
                if (n == 1) permutations.Add(new List<T>(source));
                else
                {
                    for (int i = 0; i <= n - 1; i++)
                    {
                        HeapPermutation(source, n - 1);
                        if (n % 2 == 0) Swap(i, n - 1);
                        else Swap(1, n - 1);
                    }
                }

                void Swap(int a, int b)
                {
                    T valB = source[b];
                    source[b] = source[a];
                    source[a] = valB;
                }
            }

            return permutations;
        }

        /// <summary>
        /// Gets the prime decomposition of a number.
        /// </summary>
        /// <param name="n">The number.</param>
        /// <returns>A dictionary with prime factors as keys and their powers as values.</returns>
        public static Dictionary<BigInteger, int> PrimeDecomposition(BigInteger n)
        {
            var factors = new Dictionary<BigInteger, int>();
            BigInteger divisor = 2;

            while (n != 1)
            {
                if (n % divisor == 0)
                {
                    n /= divisor;
                    if (factors.ContainsKey(divisor)) factors[divisor]++;
                    else factors.Add(divisor, 1);
                    divisor = 2;
                }
                else
                {
                    divisor++;
                }
            }

            return factors;
        }

        public static IEnumerable<int> ProperDivisors(int n) => Enumerable.Range(1, n / 2).Where(i => n % i == 0);

        public static IEnumerable<int> Quadratic(int a, int b)
        {
            for (int n = 0; ; n++)
            {
                yield return n * n + a * n + b;
            }
        }

        public static (BigInteger num, BigInteger denum) ReduceFraction(BigInteger num, BigInteger denum)
        {
            BigInteger gcd = GCD(num, denum);
            return (num / gcd, denum / gcd);
        }

        public static IEnumerable<BigInteger> RightTruncations(this BigInteger n)
        {
            int[] digits = n.Digits();
            for (int i = 1; i < digits.Length; i++)
            {
                yield return digits.Take(i).ToBigInteger();
            }
        }

        public static BigInteger SumDigitFactorials(BigInteger n)
        {
            BigInteger sum = 0;
            while (n != 0)
            {
                sum += Factorial(n % 10);
                n /= 10;
            }
            return sum;
        }

        public static BigInteger SumDigitPowers(BigInteger n, int pow)
        {
            BigInteger sum = 0;
            while (n != 0)
            {
                sum += BigInteger.Pow(n % 10, pow);
                n /= 10;
            }
            return sum;
        }

        public static BigInteger SumDigits(this BigInteger n) => n.Digits().Sum();

        /// <summary>
        /// Gets the sum of the positions of each letter of <paramref name="str"/> in the alphabet.
        /// </summary>
        /// <example><c>"abc"</c> returns <c>6</c>, <c>"sky"</c> returns <c>55</c>.</example>
        /// <param name="str">A string.</param>
        /// <returns>The alphabetical value.</returns>
        public static int ToAlphabeticalValue(this string str) => str.Sum(d => d % 32);

        public static BigInteger ToBigInteger(this IEnumerable<int> digits)
        {
            BigInteger n = 0;
            int exp = 0;

            foreach (int digit in digits.Reverse())
            {
                n += digit * BigInteger.Pow(10, exp);
                exp++;
            }

            return n;
        }

        // Zeller's congruence (cf. https://en.wikipedia.org/wiki/Zeller%27s_congruence)
        public static DayOfWeek ZellerGregorian(int year, int month, int day)
        {
            if (month == 1 || month == 2)
            {
                month += 12; // January and February are counted as months 13 and 14 of the previous year
            }

            int k = year % 100; // year of the century
            int j = year / 100; // 0-based century

            int h = (day + 13 * (month + 1) / 5 + k + k / 4 + j / 4 - 2 * j) % 7;

            return (DayOfWeek)((h + 6) % 7);
        }
    }
}