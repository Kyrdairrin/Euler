// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using Euler.Centum01.Decem01;
using Xunit;

namespace EulerTests.Centum01
{
    public class CheckDecem01
    {
        [Fact]
        public void CheckProblem001() => Assert.Equal(233168, Problem001.Solve());

        [Fact]
        public void CheckProblem002() => Assert.Equal(4613732, Problem002.Solve());

        [Fact]
        public void CheckProblem003() => Assert.Equal(6857, Problem003.Solve());

        [Fact]
        public void CheckProblem004() => Assert.Equal(906609, Problem004.Solve());

        [Fact]
        public void CheckProblem005() => Assert.Equal(232792560, Problem005.Solve());

        [Fact]
        public void CheckProblem006() => Assert.Equal(25164150, Problem006.Solve());

        [Fact]
        public void CheckProblem007() => Assert.Equal(104743, Problem007.Solve());

        [Fact]
        public void CheckProblem008() => Assert.Equal(23514624000, Problem008.Solve());

        [Fact]
        public void CheckProblem009() => Assert.Equal(31875000, Problem009.Solve());

        [Fact]
        public void CheckProblem010() => Assert.Equal(142913828922, Problem010.Solve());
    }
}
