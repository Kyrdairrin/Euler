// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using Euler.Centum01.Decem02;
using Xunit;

namespace EulerTests.Centum01
{
    public class CheckDecem02
    {
        [Fact]
        public void CheckProblem011() => Assert.Equal(70600674, Problem011.Solve());

        [Fact]
        public void CheckProblem012() => Assert.Equal(76576500, Problem012.Solve());

        [Fact]
        public void CheckProblem013() => Assert.Equal("5537376230", Problem013.Solve());

        [Fact]
        public void CheckProblem014() => Assert.Equal(837799, Problem014.Solve());

        [Fact]
        public void CheckProblem015() => Assert.Equal(137846528820, Problem015.Solve());

        [Fact]
        public void CheckProblem016() => Assert.Equal(1366, Problem016.Solve());

        [Fact]
        public void CheckProblem017() => Assert.Equal(21124, Problem017.Solve());

        [Fact]
        public void CheckProblem018() => Assert.Equal(1074, Problem018.Solve());

        [Fact]
        public void CheckProblem019() => Assert.Equal(171, Problem019.Solve());

        [Fact]
        public void CheckProblem020() => Assert.Equal(648, Problem020.Solve());
    }
}
