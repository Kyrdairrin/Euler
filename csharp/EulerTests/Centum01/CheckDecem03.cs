// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using Euler.Centum01.Decem03;
using Xunit;

namespace EulerTests.Centum01
{
    public class CheckDecem03
    {
        [Fact]
        public void CheckProblem021() => Assert.Equal(31626, Problem021.Solve());

        [Fact]
        public void CheckProblem022() => Assert.Equal(871198282, Problem022.Solve());

        [Fact]
        public void CheckProblem023() => Assert.Equal(4179871, Problem023.Solve());

        [Fact]
        public void CheckProblem024() => Assert.Equal("2783915460", Problem024.Solve());

        [Fact]
        public void CheckProblem025() => Assert.Equal(4782, Problem025.Solve());

        [Fact]
        public void CheckProblem026() => Assert.Equal(983, Problem026.Solve());

        [Fact]
        public void CheckProblem027() => Assert.Equal(-59231, Problem027.Solve());

        [Fact]
        public void CheckProblem028() => Assert.Equal(669171001, Problem028.Solve());

        [Fact]
        public void CheckProblem029() => Assert.Equal(9183, Problem029.Solve());

        [Fact]
        public void CheckProblem030() => Assert.Equal(443839, Problem030.Solve());
    }
}
