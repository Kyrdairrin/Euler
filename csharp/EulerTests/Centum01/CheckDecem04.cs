// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using Euler.Centum01.Decem04;
using Xunit;

namespace EulerTests.Centum01
{
    public class CheckDecem04
    {
        [Fact]
        public void CheckProblem031() => Assert.Equal(73682, Problem031.Solve());

        [Fact]
        public void CheckProblem032() => Assert.Equal(45228, Problem032.Solve());

        [Fact]
        public void CheckProblem033() => Assert.Equal(100, Problem033.Solve());

        [Fact]
        public void CheckProblem034() => Assert.Equal(40730, Problem034.Solve());

        [Fact]
        public void CheckProblem035() => Assert.Equal(55, Problem035.Solve());

        [Fact]
        public void CheckProblem036() => Assert.Equal(872187, Problem036.Solve());

        [Fact]
        public void CheckProblem037() => Assert.Equal(748317, Problem037.Solve());

        [Fact]
        public void CheckProblem038() => Assert.Equal(932718654, Problem038.Solve());

        [Fact]
        public void CheckProblem039() => Assert.Equal(840, Problem039.Solve());

        [Fact]
        public void CheckProblem040() => Assert.Equal(210, Problem040.Solve());
    }
}
