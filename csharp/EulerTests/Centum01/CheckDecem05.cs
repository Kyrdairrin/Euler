﻿// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using Euler.Centum01.Decem05;
using Xunit;

namespace EulerTests.Centum01
{
    public class CheckDecem05
    {
        [Fact]
        public void CheckProblem041() => Assert.Equal(7652413, Problem041.Solve());

        [Fact]
        public void CheckProblem042() => Assert.Equal(162, Problem042.Solve());

        [Fact]
        public void CheckProblem043() => Assert.Equal(16695334890, Problem043.Solve());
    }
}
