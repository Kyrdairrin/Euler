// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

using Supernova.Foundation.Core.Equality;
using System;
using System.Linq;
using System.Numerics;
using Xunit;
using static Euler.Functions;

namespace EulerTests
{
    public class FunctionsTests
    {
        [Theory]
        [InlineData(0, 0, 1)]
        [InlineData(9, 5, 126)]
        [InlineData(10, 8, 45)]
        [InlineData(5, 1, 5)]
        [InlineData(12, 10, 66)]
        public void BinomialCoefficient_ComputesBinomialCoefficient(int n, int k, int expected)
        {
            Assert.Equal(expected, BinomialCoefficient(n, k));
        }

        [Theory]
        [InlineData(192, 3, 192384576)]
        [InlineData(9, 5, 918273645)]
        public void ConcatenatedProduct_ComputedConcatenatedProduct(int num, int n, int expected)
        {
            Assert.Equal(expected, ConcatenatedProduct(num, n));
        }

        [Theory]
        [InlineData(42, 8)]
        [InlineData(192, 14)]
        [InlineData(3712, 16)]
        [InlineData(5465656, 48)]
        public void CountDivisors_GetsNumberOfDivisors(int num, int expectedCount)
        {
            Assert.Equal(expectedCount, CountDivisors(num));
        }

        [Theory]
        [InlineData(new[] { 4 }, new[] { 4 })]
        [InlineData(new[] { 1, 2, 3, 4 }, new[] { 1, 2, 3, 4 }, new[] { 2, 3, 4, 1 }, new[] { 3, 4, 1, 2 }, new[] { 4, 1, 2, 3 })]
        public void CyclicPermutations_GetsArrayCyclicPermutations(int[] array, params int[][] expected)
        {
            Assert.Equal(expected, array.CyclicPermutations());
        }

        [Theory]
        [InlineData("0", "0")]
        [InlineData("42", "42", "24")]
        [InlineData("3712", "3712", "7123", "1237", "2371")]
        public void CyclicPermutations_GetsBigIntegerCyclicPermutations(string n, params string[] expected)
        {
            Assert.Equal(expected.Select(BigInteger.Parse).ToArray(), BigInteger.Parse(n).CyclicPermutations());
        }

        [Theory]
        [InlineData(0, "1")]
        [InlineData(1, "1")]
        [InlineData(2, "2")]
        [InlineData(10, "3628800")]
        [InlineData(42, "1405006117752879898543142606244511569936384000000000")]
        [InlineData(100, "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000")]
        public void Factorial_ComputesFactorial(int n, string expected)
        {
            Assert.Equal(BigInteger.Parse(expected), Factorial(n));
        }

        [Theory]
        [InlineData(-42)]
        [InlineData(-1)]
        public void Factorial_ThrowsExceptionWithNegativeArgument(int n)
        {
            Assert.Throws<ArgumentException>(() => Factorial(n));
        }

        [Fact]
        public void Fibonacci_YieldsCorrectSequence()
        {
            Assert.Equal(new BigInteger[] { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987 }, Fibonacci().Take(17));
        }

        [Theory]
        [InlineData(42, 56, 14)]
        [InlineData(461952, 116298, 18)]
        [InlineData(7966496, 314080416, 32)]
        [InlineData(12, 0, 12)]
        [InlineData(0, 12, 12)]
        [InlineData(0, 0, 0)]
        public void GCD_ComputesGCD(int a, int b, int expectedGCD)
        {
            Assert.Equal(expectedGCD, GCD(a, b));
        }

        [Fact]
        public void IsPalindromic_ChecksIfNumberisPalindrome()
        {
            Assert.True(new[] { 0, 1, 2, 3, 4, 5, 33, 77, 99, 101, 161, 202, 272, 745547, 789424987 }.All(IsPalindromic));
            Assert.DoesNotContain(new[] { 12, 34, 890, 1233210, 993299 }, IsPalindromic);
        }

        [Theory]
        [InlineData("0123456789", 0, 9, true)]
        [InlineData("0123456", 0, 6, true)]
        [InlineData("1064523", 0, 6, true)]
        [InlineData("879", 7, 9, true)]
        [InlineData("0123456", 0, 9, false)]
        [InlineData("123456789", 1, 9, true)]
        [InlineData("193564278", 1, 9, true)]
        [InlineData("425781936", 1, 9, true)]
        [InlineData("1234567890", 1, 9, false)]
        [InlineData("0123456789", 1, 9, false)]
        [InlineData("1234506789", 1, 9, false)]
        [InlineData("1234536789", 1, 9, false)]
        [InlineData("1234556789", 1, 9, false)]
        [InlineData("12345 6789", 1, 9, false)]
        [InlineData("123456789 ", 1, 9, false)]
        [InlineData("foobar", 1, 9, false)]
        public void IsPandigital_ChecksIfIsPandigital(string number, int n_min, int n_max, bool expected)
        {
            Assert.Equal(expected, IsPandigital(number, n_min, n_max));
        }

        [Fact]
        public void IsPrime_WithNonPrimeNumbers_ReturnsFalse()
        {
            var nonPrimes = new[] { -3, -2, -1, 0, 1, 4, 6, 9, 10, 105 };
            Assert.DoesNotContain(nonPrimes, IsPrime);
        }

        [Fact]
        public void IsPrime_WithPrimeNumbers_ReturnsTrue()
        {
            var primes = new[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317 };
            Assert.True(primes.All(IsPrime));
        }

        [Theory]
        [InlineData(new[] { 74, 28 }, 1036)]
        [InlineData(new[] { 2, 40, 100, 97, 23 }, 446200)]
        [InlineData(new[] { 88, 43, 24, 50, 94 }, 13338600)]
        [InlineData(new[] { 96, 81, 81, 34, 83, 7, 96, 27 }, 25601184)]
        public void LCM_ComputesLCM(int[] numbers, int expectedLCM)
        {
            Assert.Equal(expectedLCM, LCM(numbers.Select(n => new BigInteger(n)).ToArray()));
        }

        [Theory]
        [InlineData("42", "2")]
        [InlineData("3712", "712", "12", "2")]
        public void LeftTruncations_GetsLeftTruncations(string n, params string[] expected)
        {
            Assert.Equal(expected.Select(BigInteger.Parse), BigInteger.Parse(n).LeftTruncations());
        }

        [Theory]
        [InlineData(new[] { 1 }, new[] { 1 })]
        [InlineData(new[] { 1, 2 }, new[] { 1, 2 }, new[] { 2, 1 })]
        [InlineData(new[] { 1, 2, 3 }, new[] { 1, 2, 3 }, new[] { 2, 1, 3 }, new[] { 2, 3, 1 }, new[] { 3, 2, 1 }, new[] { 3, 1, 2 }, new[] { 1, 3, 2 })]
        public void Permutations_GetsAllPermutations(int[] source, params int[][] expected)
        {
            Assert.Equal(expected, source.Permutations(), new SequenceContentEqualityComparer<int>(true));
        }

        [Theory]
        [InlineData("42", "4")]
        [InlineData("3712", "3", "37", "371")]
        public void RightTruncations_GetsRightTruncations(string n, params string[] expected)
        {
            Assert.Equal(expected.Select(BigInteger.Parse), BigInteger.Parse(n).RightTruncations());
        }

        [Fact]
        public void SumDigits_SumsDigits()
        {
            Assert.Equal(1, new BigInteger(1).SumDigits());
            Assert.Equal(1, new BigInteger(1000000).SumDigits());
            Assert.Equal(21, new BigInteger(136416).SumDigits());
            Assert.Equal(36, new BigInteger(4815162342).SumDigits());
        }

        [Theory]
        [InlineData(new[] { 0 }, "0")]
        [InlineData(new[] { 1 }, "1")]
        [InlineData(new[] { 4, 2 }, "42")]
        [InlineData(new[] { 2, 4 }, "24")]
        [InlineData(new[] { 3, 7, 1, 2 }, "3712")]
        [InlineData(new[] { 6, 5, 4, 6, 5, 1, 6, 8, 9, 4, 5, 1, 4, 6, 8, 4, 5, 4, 1, 6, 8, 4, 6, 4, 6, 8, 4, 6, 2, 3, 6, 5, 2, 2, 1, 2, 0, 0, 0, 5, 6, 4, 5, 3, 0, 5, 6, 4, 6, 5, 4, 6 }, "6546516894514684541684646846236522120005645305646546")]
        public void ToBigInteger_BuildsBigIntegerFromDigits(int[] digits, string expected)
        {
            Assert.Equal(BigInteger.Parse(expected), digits.ToBigInteger());
        }
    }
}