// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

/**
 * Returns the Fibonacci sequence
 */
fun fibonacci() = sequence {
    var terms = Pair(0, 1)
    while (true) {
        yield(terms.first)
        terms = Pair(terms.second, terms.first + terms.second)
    }
}

/**
 * Returns the prime factors of the given [number]
 */
fun primeFactors(number: Long): List<Long> {
    var n = number
    val factors = mutableListOf<Long>()
    var d = 2L
    while (n > 1) {
        while (n % d == 0L) {
            factors.add(d)
            n /= d
        }
        d++
        if (d * d > n) {
            if (n > 1) factors.add(n)
            break
        }
    }
    return factors
}

/**
 * Returns the largest prime factor of the given [number]
 */
fun largestPrimeFactor(number: Long) = primeFactors(number).last()

/**
 * Gets all the palindromes produces by the multiplication of 2 arbitrary numbers from the specifier range.
 */
fun IntProgression.getProductPalindromes(): List<Int> = this.flatMap { x -> this.map { y -> x * y } }.filter { it.isPalindrome() }

/**
 * Checks if the specified int is a palindrome.
 */
fun Int.isPalindrome() = this.toString() == this.toString().reversed()

fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)

fun lcm(numbers: List<Long>): Long {
    val (a, b) = numbers
    val rest = numbers.drop(2)

    val abLCM = a * b / gcd(a, b)

    if (rest.isEmpty()) return abLCM

    return lcm(listOf(abLCM, *rest.toTypedArray()))
}