// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class CheckAnswers {
    @Test
    fun checkProblem001() = assertEquals(233168, solve001())

    @Test
    fun checkProblem002() = assertEquals(4613732, solve002())

    @Test
    fun checkProblem003() = assertEquals(6857, solve003())

    @Test
    fun checkProblem004() = assertEquals(906609, solve004())

    @Test
    fun checkProblem005() = assertEquals(232792560, solve005())

    @Test
    fun checkProblem006() = assertEquals(25164150.0, solve006())
}