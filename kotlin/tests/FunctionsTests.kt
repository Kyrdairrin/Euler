// LICENSE NOTICE

// Copyright 2019 Nicolas Maurice

// This file is part of Euler.

// Euler is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Euler is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Euler.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class FunctionsTests {
    @Test
    fun `fibonacci produces the correct sequence`() {
        assertEquals(
            listOf(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987),
            fibonacci().take(17).toList()
        )
    }

    @Test
    fun `primeFactors with negative number returns empty list`() {
        assertTrue { primeFactors(-42).isEmpty() }
    }

    @Test
    fun `primeFactors with zero returns empty list`() {
        assertTrue { primeFactors(0).isEmpty() }
    }

    @Test
    fun `primeFactors with positive number returns prime factors`() {
        assertEquals(listOf<Long>(2, 2, 5, 5), primeFactors(100))
        assertEquals(listOf<Long>(2, 3, 7), primeFactors(42))
    }

    @Test
    fun `largestPrimeFactor with negative number throws an exception`() {
        assertFailsWith<NoSuchElementException> { largestPrimeFactor(-42) }
    }

    @Test
    fun `largestPrimeFactor with zero returns empty list`() {
        assertFailsWith<NoSuchElementException> { largestPrimeFactor(0) }
    }

    @Test
    fun `largestPrimeFactor with positive number returns prime factors`() {
        assertEquals(5, largestPrimeFactor(100))
        assertEquals(7, largestPrimeFactor(42))
    }

    @Test
    fun `Int_isPalindrome checks if the specified integer is a palindrome`() {
        assertTrue { 1.isPalindrome() }
        assertTrue { 5.isPalindrome() }
        assertTrue { 11.isPalindrome() }
        assertTrue { 22.isPalindrome() }
        assertTrue { 101.isPalindrome() }
        assertTrue { 52366325.isPalindrome() }
        assertTrue { 4571754.isPalindrome() }
        assertFalse { 10.isPalindrome() }
        assertFalse { 102102.isPalindrome() }
        assertFalse { 1233210.isPalindrome() }
        assertFalse { 123432.isPalindrome() }
        assertFalse { 42.isPalindrome() }
    }

    @Test
    fun `IntProgression_getProductPalindromes produces product palindromes`() {
        assertEquals(
            listOf(121, 242, 363, 484, 616, 737, 858, 979, 1001, 252, 444, 636, 696, 828, 888, 494, 585, 676, 767, 858, 949, 1001, 252, 434, 616, 686, 868, 525, 555, 585, 272, 464, 656, 848, 272, 323, 595, 646, 969, 252, 414, 666, 828, 323, 494, 646, 969, 1881, 252, 525, 777, 242, 484, 616, 858, 2002, 2112, 414, 575, 828, 989, 1771, 696, 888, 2112, 525, 575, 494, 676, 858, 2002, 999, 616, 868, 2772, 464, 696, 2552, 434, 868, 2112, 363, 858, 1221, 1551, 1881, 2112, 2442, 2772, 3003, 646, 2992, 525, 595, 828, 2772, 444, 555, 666, 777, 888, 999, 1221, 2442, 3663, 494, 646, 585, 858, 3003, 656, 2772, 989, 484, 616, 2112, 2332, 2552, 2772, 2992, 4004, 4224, 585, 828, 4554, 1551, 2112, 4224, 686, 3773, 969, 676, 4004, 636, 848, 2332, 4664, 5005, 5115, 5225, 5335, 5445, 616, 969, 1881, 696, 2552, 767, 868, 4774, 2772, 2112, 4224, 6336, 5005, 858, 2112, 2442, 2772, 4224, 4554, 4884, 6006, 6336, 737, 2992, 828, 4554, 6336, 949, 7227, 888, 2442, 4884, 5775, 1001, 1771, 2002, 2772, 3003, 3773, 4004, 4774, 5005, 5775, 6006, 6776, 7007, 858, 6006, 8118, 2772, 2112, 2552, 2992, 4224, 4664, 6336, 6776, 8008, 8448, 979, 1001, 2002, 3003, 4004, 5005, 6006, 7007, 8008, 9009, 5115, 5225, 2112, 4224, 6336, 8448, 5335, 1881, 2772, 3663, 4554, 5445, 6336, 7227, 8118, 9009),
            (10..100).getProductPalindromes())
        assertEquals(
            listOf(9009, 8118, 7227, 6336, 5445, 4554, 3663, 2772, 1881, 5335, 8448, 6336, 4224, 2112, 5225, 5115, 9009, 8008, 7007, 6006, 5005, 4004, 3003, 2002, 1001, 979, 8448, 8008, 6776, 6336, 4664, 4224, 2992, 2552, 2112, 2772, 8118, 6006, 858, 7007, 6776, 6006, 5775, 5005, 4774, 4004, 3773, 3003, 2772, 2002, 1771, 1001, 5775, 4884, 2442, 888, 7227, 949, 6336, 4554, 828, 2992, 737, 6336, 6006, 4884, 4554, 4224, 2772, 2442, 2112, 858, 5005, 6336, 4224, 2112, 2772, 4774, 868, 767, 2552, 696, 1881, 969, 616, 5445, 5335, 5225, 5115, 5005, 4664, 2332, 848, 636, 4004, 676, 969, 3773, 686, 4224, 2112, 1551, 4554, 828, 585, 4224, 4004, 2992, 2772, 2552, 2332, 2112, 616, 484, 989, 2772, 656, 3003, 858, 585, 646, 494, 3663, 2442, 1221, 999, 888, 777, 666, 555, 444, 2772, 828, 595, 525, 2992, 646, 3003, 2772, 2442, 2112, 1881, 1551, 1221, 858, 363, 2112, 868, 434, 2552, 696, 464, 2772, 868, 616, 999, 2002, 858, 676, 494, 575, 525, 2112, 888, 696, 1771, 989, 828, 575, 414, 2112, 2002, 858, 616, 484, 242, 777, 525, 252, 1881, 969, 646, 494, 323, 828, 666, 414, 252, 969, 646, 595, 323, 272, 848, 656, 464, 272, 585, 555, 525, 868, 686, 616, 434, 252, 1001, 949, 858, 767, 676, 585, 494, 888, 828, 696, 636, 444, 252, 1001, 979, 858, 737, 616, 484, 363, 242, 121),
            (100 downTo 10).getProductPalindromes())
    }
}