# LICENSE NOTICE

# Copyright 2019 Nicolas Maurice

# This file is part of Euler.

# Euler is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Euler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Euler.  If not, see <http://www.gnu.org/licenses/>.

# END LICENSE NOTICE

from math import gcd

def fibonacci():
    yield 1
    yield 1

    a, b = 1, 1

    while True:
        yield a + b
        a, b = b, a + b


def largest_prime_factor(num):
    d = 2

    while d * d <= num:
        if num % d != 0:
            d += 1
        else:
            num /= d

    return num


def is_palindrome(n):
    return str(n) == str(n)[::-1]  # see https://stackoverflow.com/a/509295


def lcm(numbers):
    a = numbers[0]
    b = numbers[1]
    rest = numbers[2:]

    ab_lcm = a * b // gcd(a, b)

    if (len(rest) == 0):
        return ab_lcm

    return lcm([ab_lcm, *rest])