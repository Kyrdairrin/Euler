# LICENSE NOTICE

# Copyright 2019 Nicolas Maurice

# This file is part of Euler.

# Euler is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Euler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Euler.  If not, see <http://www.gnu.org/licenses/>.

# END LICENSE NOTICE
import unittest
import problem001, problem002, problem003, problem004, problem005

class Test_CheckAnswers(unittest.TestCase):

    def test_check_problem001(self):
        self.assertEqual(233168, problem001.solve())

    def test_check_problem002(self):
        self.assertEqual(4613732, problem002.solve())

    def test_check_problem003(self):
        self.assertEqual(6857, problem003.solve())

    def test_check_problem004(self):
        self.assertEqual(906609, problem004.solve())

    def test_check_problem005(self):
        self.assertEqual(232792560, problem005.solve())
