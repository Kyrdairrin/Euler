# LICENSE NOTICE

# Copyright 2019 Nicolas Maurice

# This file is part of Euler.

# Euler is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Euler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Euler.  If not, see <http://www.gnu.org/licenses/>.

# END LICENSE NOTICE

import unittest
import itertools as it
import functions as f

class Test_Functions(unittest.TestCase):

    def test_fibonacci(self):
        seq = list(it.islice(f.fibonacci(), 10))
        self.assertEqual(seq, [1, 1, 2, 3, 5, 8, 13, 21, 34, 55])


    def test_largest_prime_factor(self):
        cases = [(42, 7), (762562, 8867), (56416541, 100207), (600851475143, 6857)]
        for num, expected in cases:
            with self.subTest():
                self.assertEqual(f.largest_prime_factor(num), expected)


    def test_is_palindrome(self):
        truethy = [0, 1, 2, 3, 4, 5, 33, 77, 99, 101, 161, 202, 272, 745547, 789424987]
        falsy = [12, 34, 890, 1233210, 993299]
        for num in truethy:
            with self.subTest():
                self.assertTrue(f.is_palindrome(num))
        for num in falsy:
            with self.subTest():
                self.assertFalse(f.is_palindrome(num))


    def test_lcm(self):
        cases = [([74, 28], 1036), ([2, 40, 100, 97, 23], 446200), ([88, 43, 24, 50, 94], 13338600), ([96, 81, 81, 34, 83, 7, 96, 27], 25601184)]
        for numbers, expected in cases:
            with self.subTest():
                self.assertEqual(f.lcm(numbers), expected)